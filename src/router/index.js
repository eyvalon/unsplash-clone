import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Search from "@/views/Search.vue";
import Topics from "@/views/Topics.vue";
import Topic from "@/views/Topic.vue";
import PageNotFound from "@/views/PageError.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/s/:slug",
      name: "Search",
      component: Search
    },
    {
      path: "/t",
      name: "Topic",
      component: Topic
    },
    {
      path: "/t/:slug",
      name: "Topics",
      component: Topics
    },
    {
      path: "/404",
      component: PageNotFound
    },

    { path: "*", redirect: "/404" }
  ]
});

export default router;
