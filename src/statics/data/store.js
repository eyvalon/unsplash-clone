import Vue from "vue";

export const store = Vue.observable({
  features: [
    {
      title: "Sustainability",
      description:
        "From photos of plastic pollution to shots of peaceful moments in nature — we’ve teamed up with Boxed Water Is Better to curate a special Sustainability topic to celebrate our planet. And from now until August 31st, as part of their #BetterPlanet initiative, you can submit your best sustainability photos for a chance to be featured. For every photo submitted, Boxed Water will also plant 2 trees in U.S. National forests, up until 5,000 trees.",
      author: "Boxed Water Is Better",
      status: "Open",
      contributions: "499",
      top_contributors: "List",
      Logo:
        "https://images.unsplash.com/profile-1544707963613-16baf868f301?auto=format&fit=crop&w=64&h=64&q=60&crop=faces&bg=fff",
      cover_picture:
        "https://images.unsplash.com/40/rqZBrx1WRsCtkqGSVBK6_IMG_0063.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjF9&auto=format&fit=crop&w=438&h=136.875&q=60",
      date: "September 1, 2020 at 9:59 AM",
      type: "features"
    },
    {
      title: "COVID-19",
      description:
        "Together, we’re building a library of visuals that reflect our new reality due to the COVID-19 pandemic — from from shots empty public spaces to handmade face masks. Now featuring creative responses from the United Nations’ Global Open Brief.",
      author: "United Nations COVID-19 Response",
      status: "Open",
      contributions: "1.9k",
      top_contributors: "List",
      Logo:
        "https://images.unsplash.com/profile-1544707963613-16baf868f301?auto=format&fit=crop&w=64&h=64&q=60&crop=faces&bg=fff",
      cover_picture:
        "https://images.unsplash.com/45/QDSMoAMTYaZoXpcwBjsL__DSC0104-1.jpg?ixlib=rb-1.2.1&auto=format&fit=crop&w=438&h=136.875&q=60",
      date: "",
      type: "topic"
    },
    {
      title: "Animals",
      description:
        "Exotic wildlife, pet kittens — and everything in between. Uncover the beauty of the animal kingdom through your screen.",
      author: "SQUARESPACE",
      status: "Open",
      contributions: "3.2k",
      top_contributors: "List",
      Logo:
        "https://images.unsplash.com/profile-1544707963613-16baf868f301?auto=format&fit=crop&w=64&h=64&q=60&crop=faces&bg=fff",
      cover_picture:
        "https://images.unsplash.com/44/MIbCzcvxQdahamZSNQ26_12082014-IMG_3526.jpg?ixlib=rb-1.2.1&auto=format&fit=crop&w=438&h=136.875&q=60",
      date: "",
      type: "topic"
    },
    {
      title: "Interiors",
      description:
        "Whether it’s a peaceful bedroom or a cluttered kitchen — photographs of our spaces tell the story of who we are.",
      author: "SQUARESPACE",
      status: "Open",
      contributions: "1.1k",
      top_contributors: "List",
      Logo:
        "https://images.unsplash.com/profile-1544707963613-16baf868f301?auto=format&fit=crop&w=64&h=64&q=60&crop=faces&bg=fff",
      cover_picture:
        "https://images.unsplash.com/photo-1432302250330-cce014453cd6?ixlib=rb-1.2.1&auto=format&fit=crop&w=438&h=136.875&q=60",
      date: "",
      type: "topic"
    },
    {
      title: "Work From Home",
      description:
        "As we spend more time than ever indoors, our daily routines, work schedules and family dynamics have all shifted. From conference calls in your bedroom, to keeping your kids busy with crafts as you work. It’s time to define the new visual language for working from home.",
      author: "SQUARESPACE",
      status: "Closed",
      contributions: "297",
      top_contributors: "List",
      Logo:
        "https://images.unsplash.com/profile-1544707963613-16baf868f301?auto=format&fit=crop&w=64&h=64&q=60&crop=faces&bg=fff",
      cover_picture:
        "https://images.unsplash.com/photo-1452457779869-0a9ebbbdee99?ixlib=rb-1.2.1&auto=format&fit=crop&w=438&h=136.875&q=60",
      date: "May 29, 2020 at 9:59 AM",
      type: "topic"
    }
  ]
});

export const mutations = {};
