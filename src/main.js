import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VueMeta from "vue-meta";
import VueAxios from "./plugins/axios";

Vue.config.productionTip = false;
Vue.use(VueAxios);
Vue.use(VueMeta);

new Vue({
  router,
  linkActiveClass: "active",
  linkExactActiveClass: "exact-active",
  render: (h) => h(App)
}).$mount("#app");
