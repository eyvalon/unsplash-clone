import config from "@/statics/data/API.js";

export default {
  methods: {
    getItems: function() {
      const requestOptions = {
        headers: { Authorization: `Client-ID ${config.client_ID}` }
      };

      return fetch(
        config.api + `?query=${this.$route.params.slug}&per_page=28`,
        requestOptions
      ).then((res) => res.json());
    }
  },
  created: function() {
    this.getItems().then((data) => (this.items = data.results));
  },
  watch: {
    "$route.params.slug": function() {
      this.items = [];
      this.getItems().then((data) => (this.items = data.results));
    }
  }
};
