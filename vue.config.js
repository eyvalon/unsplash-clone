module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/styles/_variables.scss";`
      }
    }
  },

  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "Unsplash";
      args[0].meta = {
        viewport: "width=device-width,initial-scale=1,user-scalable=no"
      };

      return args;
    });
  }
};
