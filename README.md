### Description

This project is developed to demonstrate how you can replicate a VueJs web application for [Unsplash](https://unsplash.com/) from scratch without depending on bootstrap frameworks but pure CSS pre-preprocessor (SCSS).

> This project is written for educational purposes and do not plan to abuse the name or the use of API from [Unsplash](https://unsplash.com/).

### Sample Pictures

![Alt Text](./images/landingPage.png)

| Sample Picture                      | Sample Picture                      |
| ----------------------------------- | ----------------------------------- |
| ![Alt Text](./images/topicList.png) | ![Alt Text](./images/topicPage.png) |

### Various tools used

- CSS Grid
- Flexbox
- Creating reusable components
- Unsplash API to search for queries

### Pages implemented

Since this is a replication for some of the pags ( not a full clone ) of Unsplash, the pages that I decided to replicate are:

- Home `/`
- Topics `/t/`
- Sub topic pages `/t/:slug`
- Search Page `/s/`
- Any other page would be redirected to `/404`

### Note:

This project is setup by storing the CLIENT_ID of [Unsplash API](https://unsplash.com/documentation/) to a file named `API.js` located at `src/statics/data/`. To further ensure that the CLIENT_ID is secured, it is better to store in a `.env` file.

## Project setup

```
npm install
```

## Running project

To run on default port 8080:

```
npm run serve
```

To run on a certain port (i.e. 3000):

```
npm run serve -- --port 3000
```
